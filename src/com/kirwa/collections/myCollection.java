package com.kirwa.collections;

import java.util.*;

/**
 * Created by KIRTESH WANI on 3/16/15.
 */
public class myCollection {

    public static void main(String[] args) {
        Map<String,String> myMap = new HashMap<String, String>();

        myMap.put("key1","Value1");
        myMap.put("key2","Value2");
        myMap.put("key3","Value3");
        myMap.put("key4","Value4");
        myMap.put("key5","Value5");

        Set<Map.Entry<String, String>> entries = myMap.entrySet();
        for (Map.Entry<String, String> entry : entries) {
            System.out.println(entry.getKey());
            System.out.println(entry.getValue());
        }

        String sentence = "Hello All My Name is Kirtesh, I Love My India";
    }
}
