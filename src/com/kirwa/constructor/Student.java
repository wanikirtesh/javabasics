package com.kirwa.constructor;

/**
 * ****************************
 * Created by Kirtesh Wani on 16-06-2015.
 * for JavaBasics
 * *****************************
 */
public class Student {
    private String FName;
    private String LName;
    private int aClass;

    public void setFName(String FName) {
        this.FName = FName;
    }

    public void setLName(String LName) {
        this.LName = LName;
    }

    public void setClass(int aClass) {
        this.aClass = aClass;
    }

    public String getFName() {
        return FName;
    }

    public String getLName() {
        return LName;
    }
}
