package com.kirwa.constructor;

/**
 * ****************************
 * Created by Kirtesh Wani on 16-06-2015.
 * for JavaBasics
 * *****************************
 */
public class NewStudent {
    private final String fName;
    private final String lName;
    private int aClass;

    public NewStudent(String fName, String lName) {
        this.fName= fName;
        this.lName=lName;
    }

    public void setClass(int aClass) {
        this.aClass = aClass;
    }

    public String getFName() {
        return fName;
    }

    public String getLName() {
        return lName;
    }
}
