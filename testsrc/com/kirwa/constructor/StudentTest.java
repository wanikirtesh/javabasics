package com.kirwa.constructor;

import org.junit.Assert;
import org.junit.Test;

/**
 * ****************************
 * Created by Kirtesh Wani on 16-06-2015.
 * for JavaBasics
 * *****************************
 */
public class StudentTest {
    @Test
    public void simple_student_object_creation_without_constructor(){
        Student s1 = new Student();
        s1.setFName("Kirtesh");
        s1.setLName("Wani");
        s1.setClass(5);
        System.out.println("Name of Sudent is" + s1.getFName() + " " + s1.getLName());
        Assert.assertEquals("Kirtesh Wani",s1.getFName() + " " + s1.getLName());
    }

    @Test
    public void simple_student_object_creation_with_constructor(){
        NewStudent s1 = new NewStudent("Kirtesh","Wani");
        s1.setClass(5);
        Assert.assertEquals("Kirtesh Wani",s1.getFName() + " " + s1.getLName());
    }
}
